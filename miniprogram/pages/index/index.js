//index.js
const app = getApp()

const ApiConnector = require("../../helpers/ApiConnector").ApiConnector;

Page({
  
  data: {
    id: 1,
    namakamu:"Kuda",
    avatarUrl: './user-unlogin.png',
    userInfo: {},
    logged: false,
    takeSession: false,
    requestResult: '',
    bgColor: "",
    products: [],
    updateCart:[],
  }, 

  session: function(){
    let namahewan = this.data.namakamu;
    let id = this.data.id;
    let objectHewan = {
      id: id,
      namahewan: namahewan
    };

    let update = this.data.updateCart.concat(objectHewan);
    this.setData({
      updateCart: update,
    })

    try {
      wx.setStorageSync('key', this.data.updateCart)
    } catch (e) { }
    console.log(this.data.updateCart, "SAPI")

    
    wx.navigateTo({
      url: '../../pages/productdiscount/productdiscount?sapi=iniadalahhewan',
    })
  },

  goDetail: function(e) {
    wx.navigateTo({
      url: '../../pages/productdetail/productdetail?title='+e.target.dataset.hi.goods_name+'&price='+e.target.dataset.hi.shop_price+'',
    })
    console.log(e.target.dataset.hi.goods_id)
  },

  onLoad: function() {

    // ApiConnector({
    //   url: "/fresh/Product/productList",
    //   data: JSON.stringify({layout_id: 1}),
    //   method: "POST",
    //   success: res => {
    //     this.setData({
    //       products: res.data.data
    //     })               
    //     console.log(res.data.data)
    //   },
      
    //   // function (jsonObj) { console.log("success?", jsonObj); },
    //   // error: function(err) { console.log("error? ", err); },
    //   complete: function() { console.log("complete function?"); },
    //   beforeSend: function() { console.log("before send?"); }
    // });

   


    if (!wx.cloud) {
      wx.redirectTo({
        url: '../chooseLib/chooseLib',
      })
      return
    }

    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              this.setData({
                avatarUrl: res.userInfo.avatarUrl,
                userInfo: res.userInfo
              })
            }
          })
        }
      }
    })
  },

  onGetUserInfo: function(e) {
    if (!this.data.logged && e.detail.userInfo) {
      this.setData({
        logged: true,
        avatarUrl: e.detail.userInfo.avatarUrl,
        userInfo: e.detail.userInfo
      })
    }
  },

  onGetOpenid: function() {
    // 调用云函数
    wx.cloud.callFunction({
      name: 'login',
      data: {},
      success: res => {
        console.log('[云函数] [login] user openid: ', res.result.openid)
        app.globalData.openid = res.result.openid
        wx.navigateTo({
          url: '../userConsole/userConsole',
        })
      },
      fail: err => {
        console.error('[云函数] [login] 调用失败', err)
        wx.navigateTo({
          url: '../deployFunctions/deployFunctions',
        })
      }
    })
  },

  // 上传图片
  doUpload: function () {
    // 选择图片
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {

        wx.showLoading({
          title: '上传中',
        })

        const filePath = res.tempFilePaths[0]
        
        // 上传图片
        const cloudPath = 'my-image' + filePath.match(/\.[^.]+?$/)[0]
        wx.cloud.uploadFile({
          cloudPath,
          filePath,
          success: res => {
            console.log('[上传文件] 成功：', res)

            app.globalData.fileID = res.fileID
            app.globalData.cloudPath = cloudPath
            app.globalData.imagePath = filePath
            
            wx.navigateTo({
              url: '../storageConsole/storageConsole'
            })
          },
          fail: e => {
            console.error('[上传文件] 失败：', e)
            wx.showToast({
              icon: 'none',
              title: '上传失败',
            })
          },
          complete: () => {
            wx.hideLoading()
          }
        })

      },
      fail: e => {
        console.error(e)
      }
    })
  },

})




// changeColor: function (target = "blue") {
  //   this.setData({
  //     bgColor: target,
  //   });
  // },
  // onLoad: function() {
  //     setTimeout(this.changeColor.bind(this), 5000);

  // onLoad: function() {
  //   console.log("Page onLoad this?", this);
  //   Page.this = this;
  //     setTimeout(function(e) {
  //       console.log(e);
  //       Page.this.setData({
  //         bgColor: "blue",
  //       });
  //     }, 5000);
    // wx.request({
    //   url: 'https://api.npoint.io/047f6da0f63a3887a54d/posts', //This value for demonstration purposes only is not a real API URL.
    //   data: {
    //     x: '',
    //     y: ''
    //   },
    //   header: {
    //     'content-type': 'application/json' // Default value
    //   },
    //   success: res => {
    //     this.setData({
    //       sapi: res.data.name
    //     })
    //     console.log(res.data)
    //   }
    // })