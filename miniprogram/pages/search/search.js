// miniprogram/pages/search/search.js
const ApiConnector = require("../../helpers/ApiConnector").ApiConnector;

Page({

  
  /**
   * Page initial data
   */
  data: {
    // jenis: [
    //   'kol', 'sawi', 'bayam', 'wortel', 'kentang', 'paprika', 'kangkung', 'selada'
    // ]
    products: []
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    ApiConnector({
      url: "/fresh/Product/productList",
      data: JSON.stringify({layout_id: 1}),
      method: "POST",
      success: res => {
        this.setData({
          products: res.data.data
        })               
        console.log(res.data.data)
      },
      
      // function (jsonObj) { console.log("success?", jsonObj); },
      // error: function(err) { console.log("error? ", err); },
      complete: function() { console.log("complete function?"); },
      beforeSend: function() { console.log("before send?"); }
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {

  }
})