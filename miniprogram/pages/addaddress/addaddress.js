// miniprogram/pages/addaddress/addaddress.js
Page({

  data: {
    province: ['Riau Islands', 'North Sumatra', 'West Java', 'East Borneo'],
    objectArray: [
      {
        id: 0,
        name: 'Riau Islands'
      },
      {
        id: 1,
        name: 'North Sumatra'
      },
      {
        id: 2,
        name: 'West Java'
      },
      {
        id: 3,
        name: 'East Borneo'
      }
    ],    
    city: ['Batam', 'Tanjung Pinang', 'Natuna', 'Belakang Padang'],
    objectArray: [
      {
        id: 0,
        name: 'Batam'
      },
      {
        id: 1,
        name: 'Tanjung Pinang'
      },
      {
        id: 2,
        name: 'Natuna'
      },
      {
        id: 3,
        name: 'Belakang Padang'
      }
    ], 
    district: ['Batu Aji', 'Batam Center', 'Sekupang', 'Tg. Piayu'],
    objectArray: [
      {
        id: 0,
        name: 'Batu Aji'
      },
      {
        id: 1,
        name: 'Batam Center'
      },
      {
        id: 2,
        name: 'Sekupang'
      },
      {
        id: 3,
        name: 'Tg. Piayu'
      }
    ],

    indexcity: 0,
    indexprovince: 0,
    indexdistrict:0,  
  },

  bindPickerProvince: function (e) {
    this.setData({
      indexprovince: e.detail.value
    })
  },

  bindPickerCity: function (e) {
    this.setData({
      indexcity: e.detail.value
    })
  },

  bindPickerDistrict: function (e) {
    this.setData({
      indexdistrict: e.detail.value
    })
  },


  onLoad: function (options) {

  },


  onReady: function () {

  },


  onShow: function () {

  },


  onHide: function () {

  },


  onUnload: function () {

  },


  onPullDownRefresh: function () {

  },


  onReachBottom: function () {

  },


  onShareAppMessage: function () {

  }
})