// miniprogram/pages/productdetail/productdetail.js
const ApiConnector = require("../../helpers/ApiConnector").ApiConnector;

Page({

  /**
   * Page initial data
   */
  data: {
    products: [],
    showmodals: "",
    addtocartsucss:"",
    close:"",
    counter:1,
    title:'{{title}}',
    price:'{{price}}',
  },

  addItem:function(){
    this.setData({
      counter: this.data.counter+1,
    })
    console.log(this.data.counter);
  },

  remItem:function(){
    if((this.data.counter >= 2))
      this.setData({
        counter: this.data.counter-1,
      }); else this.setData({
        counter: this.data.counter,
      });
      console.log(this.data.counter);
  },

  show: function(){
    this.setData({
      addtocartsucss: "none",
      showmodals: "block",
    });
    
  },

  close: function(){
    this.setData({
      showmodals: "none",
    });
  },

  addtocartsuccess: function(){
    this.setData({
      addtocartsucss: "block",
    })
    setTimeout(this.close.bind(this), 1500);
  },

  goDetail: function(e) {
    wx.redirectTo({
      url: '../../pages/productdetail/productdetail?title='+e.target.dataset.hi.goods_name+'&price='+e.target.dataset.hi.shop_price+'',
    })
    console.log(e.target.dataset.hi.goods_id)
  },

  buyNow: function (e){
    wx.navigateTo({
      url: '../../pages/checkout/checkout?title='+this.data.title+'&price='+this.data.price+'',
    })
    console.log(this.data.title);  
  },
  
  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    this.setData({
      title: options.title,
      price: options.price
    })

    ApiConnector({
      url: "/fresh/Product/productList",
      data: JSON.stringify({layout_id: 1}),
      method: "POST",
      success: res => {
        this.setData({
          products: res.data.data
        })               
        console.log(res.data.data)
      },
      
      // function (jsonObj) { console.log("success?", jsonObj); },
      // error: function(err) { console.log("error? ", err); },
      complete: function() { console.log("complete function?"); },
      beforeSend: function() { console.log("before send?"); }
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {

  }
})