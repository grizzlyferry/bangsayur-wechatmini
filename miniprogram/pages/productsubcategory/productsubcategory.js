// miniprogram/pages/productsubcategory/productsubcategory.js
const ApiConnector = require("../../helpers/ApiConnector").ApiConnector;
Page({

  /**
   * Page initial data
   */
  data: {
    category: [],
    products: []
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    ApiConnector({
      url: "/fresh/Product/productList",
      data: JSON.stringify({layout_id: 1}),
      method: "POST",
      success: res => {
        this.setData({
          category: res.data.data,
          products: res.data.data
        })
      },
      complete: function() { console.log("complete function?"); },
      beforeSend: function() { console.log("before send?"); }
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {

  }
})