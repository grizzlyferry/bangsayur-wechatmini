const serverAddr = require("../constants/FreshServer.js").serverAddress;
let retryCount = 0;
function errorPopup (errorMsg, title = "Error", confirmText = "OK", retryFunc = null) {
  wx.showModal({
    title: title,
    content: errorMsg,
    showCancel: false,
    confirmText: confirmText,
    complete: function() {
      if(retryFunc !== null) retryFunc();
    }
  });
}
function errorPage (errorMsg, title = "Error", allowBack = true) {
  if(allowBack) {
    wx.navigateTo({
      url: '../error/error?errMsg='+errorMsg+'&title='+title+'&allowBack=1',
    });
  } else {
    wx.redirectTo({
      url: '../error/error?errMsg='+errorMsg+'&title='+title+'&allowBack=0',
    });
  }
}
function ApiConnector (opt) {
  let errFunc = opt.error ? opt.error : errorPopup;
  let method = opt.method ? opt.method : "POST";
  let defaultHeader = {
    Accept: "application/json",
    'Content-Type': "application/json",
    Authorization: "Device Test Platform WeChat Bearer Token"
  };
  let systemInfo = wx.getSystemInfo({
    complete: (res) => {
      console.log(res);
    },
  });
  let header = opt.header ? Object.assign(opt.header, defaultHeader) : defaultHeader;
  if(!opt.success) {
    errorPage("Please update your app to fix this problem!", "Error!\nCode: 10", false);
  }
  wx.request({
    url: serverAddr+opt.url,
    data: opt.data,
    header: header,
    method: method,
    dataType: 'json',
    success: opt.success,
    fail: (retryCount >= 5)
      ? errorPage.bind(this, "Connection Failed or Response Incorrect!\nYou retried "+retryCount+" times.\nMaybe updating the apps will help you!", "Error!\nCode: -3", false)
      : errFunc.bind(this, "Connection Failed or Response Incorrect!", "Error!\nCode: -2", "Retry", function() {
          retryCount++;
          ApiConnector(opt);
        })
  });
}
module.exports = {
  ApiConnector: ApiConnector
}