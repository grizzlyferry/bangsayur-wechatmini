// components/addresscard/addresscard.js
Component({
  /**
   * Component properties
   */
  properties: {
    title : String,
    type: String,
    edit: String,
    checked: String,
  },

  /**
   * Component initial data
   */
  data: {

  },

  /**
   * Component methods
   */
  methods: {

  }
})
