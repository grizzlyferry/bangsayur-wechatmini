// components/appbarchart/appbarchart.js
Component({
  /**
   * Component properties
   */
  properties: {
    
  },

  /**
   * Component initial data
   */
  data: {
    src: '../../images/back.png',
    chart: '../../images/shopchart.png'
  },

  /**
   * Component methods
   */
  methods: {
    goBack: function(event) {
      wx.navigateBack({
        complete: (res) => {},
      })
    },
    gotoCart: function(event) {
      wx.navigateTo({
        url: '../../pages/chart/chart',
      })
    },
  }
})

