// components/banner/banner.js
Component({
  /**
   * Component properties
   */
  properties: {

  },

  /**
   * Component initial data
   */
  data: {
    src : '../../images/banner.png'
  },

  /**
   * Component methods
   */
  methods: {

  }
})
