// components/viewlistmenu/listviewmenu.js
Component({
  /**
   * Component properties
   */
  properties: {
    title : String,
    nav : String,

  },

  /**
   * Component initial data
   */
  data: {

  },

  /**
   * Component methods
   */
  methods: {

    viewAll: function(events) {
      console.log('form triggers a submit event, carrying the following data: ', events.detail.value)
      wx.navigateTo({
        url: '../../pages/productdiscount/productdiscount',
        complete: (res) => {},
        events: events,
        fail: (res) => {},
        success: (result) => {
          console.log('form triggers a submit event, carrying the following data: ', events.detail.value)
        },
      })
    },
  }
  
})
