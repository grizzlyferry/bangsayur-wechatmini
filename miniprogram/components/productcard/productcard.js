// components/productcard/productcard.js
Component({
  /**
   * Component properties
   */
  properties: {
    product: Object,
  },

  /**
   * Component initial data
   */
  data: {

  },

  /**
   * Component methods
   */
  methods: {

  }
})
