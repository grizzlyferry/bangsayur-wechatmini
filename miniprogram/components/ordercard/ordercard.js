// components/ordercard/ordercard.js
Component({
  /**
   * Component properties
   */
  properties: {
    title: String,
    price: String,
  },

  /**
   * Component initial data
   */
  data: {

  },

  /**
   * Component methods
   */
  methods: {

  }
})
