// components/appbarsearch/appbarsearch.js
Component({

  
  
  /**
   * Component properties
   */
  properties: {
    
  },

  /**
   * Component initial data
   */
  data: {
    src: '../../images/back.png'
  },

  /**
   * Component methods
   */
  methods: {
    goHome: function(event) {
      wx.redirectTo({
        url: '../../pages/index/index',
      })
    },
    
  }
})
