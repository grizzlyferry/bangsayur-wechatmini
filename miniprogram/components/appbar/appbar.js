// components/appbar/appbar.js
Component({

  /**
   * Component properties
   */
  properties: {
    // ini props untuk location di appbar hijau
    city:String,
    color:String
  },

  /**
   * Component initial data
   */
  
  data: {
// ini data untuk icon location link di appbar hijau
    src:'../../images/place.png'
  },


  /**
   * Component methods
   */
  methods: {
    _propertyChanged: function(asd) {
      console.log("property changed? ", asd);
    },

    searchSubmit: function(events) {

      console.log('form triggers a submit event, carrying the following data: ', events.detail.value)
      wx.navigateTo({
        url: '../../pages/productdiscount/productdiscount',
        complete: (res) => {},
        events: events,
        fail: (res) => {},
        success: (result) => {
          console.log('form triggers a submit event, carrying the following data: ', events.detail.value)
        },
      })
    },

  },
    

})


