// components/categorylistmenu/categorylistmenu.js
Component({
  /**
   * Component properties
   */
  properties: {
    category: Object,
  },

  /**
   * Component initial data
   */
  data: {

  },

  /**
   * Component methods
   */
  methods: {

  }
})
