// components/productdetailcard/productdetailcard.js
Component({
  /**
   * Component properties
   */
  properties: {
    namaproduk: String,
    berat:String,
    keterangan:String,
    harga:String
  },

  /**
   * Component initial data
   */
  data: {
    src: 'https://cdn-asset.jawapos.com/wp-content/uploads/2019/04/sayur-hijau-640x447.jpg'
  },

  /**
   * Component methods
   */
  methods: {

  }
})
